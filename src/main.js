// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// Vue轮播插件
import VueAwesomeSwiper from 'vue-awesome-swiper'
// 轮播样式
require('swiper/dist/css/swiper.css')

import 'styles/reset.css'//初始化样式文件
import 'styles/border.css'//解决移动端 一像素问题

// vue轮播插件使用
Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
